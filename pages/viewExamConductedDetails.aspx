﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/examMaster.master" AutoEventWireup="true" CodeFile="viewExamConductedDetails.aspx.cs" Inherits="pages_viewExamConductedDetails" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Exam Details</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyStart" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body2" Runat="Server">
     <div class="content">
        <div class="container-fluid">


              <div class="row">
                   <div class="col-lg-6 col-lg-offset-3">
                  <div class="card">
                        <div class="content text-center">
                           <asp:Label ID="lbl21" Font-Size="X-Large" Text="Result:" runat="server" ></asp:Label>
                            <asp:Label ID="lblStatus" Font-Size="X-Large" Font-Bold="true" runat="server" ></asp:Label>
                        </div>
                    </div>
                       </div>
                 </div>

      <div class="row">
                
                 
                                     <div class="col-lg-12 col-sm-12 col-md-12 ">
                  


                                          <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading" style="background:white">
      <h4 class="panel-title">
        <a data-toggle="collapse"  href="#collapse1">
            <asp:Label ID="lblCandidateEmail" Font-Size="X-Large" runat="server" ></asp:Label>&nbsp<i class="ti-arrow-circle-down" style="color: black"></i></a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body"><table class="table">
          <tr>
              <td>Name</td>
                <td> <asp:Label ID="lblName" Font-Bold="true" runat="server" ></asp:Label></td>
                  <td>Experience</td>
                <td> <asp:Label ID="lblExp" Font-Bold="true" runat="server" ></asp:Label></td>
                <td>Contact</td>
                <td> <asp:Label ID="lblContact" Font-Bold="true"  runat="server" ></asp:Label></td>
          </tr>

          

            <tr>
                <td>Birth Date</td>
                <td> <asp:Label ID="lblBirthDate" Font-Bold="true" runat="server" ></asp:Label></td>
              <td>Arranged At</td>
                <td> <asp:Label ID="lblArrangedAt" Font-Bold="true" runat="server" ></asp:Label></td>
                  
               <td>Remark</td>
                <td> <asp:Label ID="lblRemark" Font-Bold="true" runat="server" ></asp:Label></td>
          </tr>
           <tr>
              
                  <td>Location</td>
                <td> <asp:Label ID="lblLocation" Font-Bold="true" runat="server" ></asp:Label></td>
                <td>Position</td>
                <td> <asp:Label ID="lblPosition" Font-Bold="true" runat="server" ></asp:Label></td>
                 <td>Department</td>
                <td> <asp:Label ID="lblDepartment" Font-Bold="true" runat="server" ></asp:Label></td>
          </tr>

          
           
                              </table></div>
     
    </div>
  </div>
</div> 
                </div>
                 </div>



           



             <div class="row">
                   <div class="col-lg-12">
                  <div class="card">
                     

                        <div class="content">
                       


                            <asp:GridView ID="examDetails" CssClass="table" runat="server" EmptyDataText="No records Found">  
</asp:GridView> 

                            <%--<table class="table">
                                <caption>Exam Details</caption>
                                <tr>
                                    <td>
                                        Sync At :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSynctAt" Font-Bold="true" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        Is Psychometric Completed? :
                                    </td>
                                    <td>
                                         <asp:Label ID="lbl_is_psychometric_completed" Font-Bold="true" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                          Is Aptitude Completed? :
                                    </td>
                                    <td>
                                         <asp:Label ID="lbl_is_jombay_aptitude_completed" Font-Bold="true" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </table>--%>
                        </div>
                    </div>
                       </div>
                 </div>

              <div class="row">
                   <div class="col-lg-12">
                  <div class="card">
                       
                        <div class="content">
                            <label>HR Section</label>
                       <asp:GridView ID="hrSection" CssClass="table" runat="server" EmptyDataText="No records Found">  
</asp:GridView> 
                           
                        </div>
                    </div>
                       </div>
                 </div>


            </div>
       </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Footer" Runat="Server">
</asp:Content>

