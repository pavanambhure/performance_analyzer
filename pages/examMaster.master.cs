﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_examMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblUserName.Text = DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]).ToUpper().Replace("@SANJEEVGROUP.COM", "");

        //Pavan Ambhure

        //To check the master menus
    
        string pageName = Path.GetFileName(Request.Path);

        if (pageName == "Form_approvalList.aspx" || pageName == "Form_ChangeStatus.aspx")
        {
            //NavigationId.Visible = false;
            //logoLink.HRef = "#";
            ////Session[PublicMethods.ConstUserEmail] = "administrator";

            //statusChangeList.Visible = true;
        }

        imgUserProfile.ImageUrl = "https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=" + DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]) + "&UA=0&size=HR64x64&sc=1468233338850";

        if (Session[PublicMethods.ConstUserEmail].ToString() == "Admin@sanjeevgroup.com")
        {
            
         
            //liMaster.Visible = true;
            //interviewLink.HRef = "#";
          
        }
        else
        {
            lbl_ISadmin.ForeColor = System.Drawing.ColorTranslator.FromHtml("#EB5E28");
            lbl_ISadmin.Text = "";
        }
    }
}
