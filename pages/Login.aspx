﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

     <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <style>
        body {
            padding-top: 60px;
        }
    </style>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="style/login-register.css" rel="stylesheet" />
    <link href="style/font-awesome.css" rel="stylesheet" />
    <script src="jquery/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>
    <script src="style/login-register.js" type="text/javascript"></script>
    <title>Login</title>


    
    

   
    


</head>
   <body style="background-image: linear-gradient(to right,#563d7c , white);width: 100%; height: 100%;">







   


 










    <form id="form1" runat="server">


        <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>





         



        <div class="container">
            <div class="login" id="loginModal">

              
                <div class="modal-dialog login animated">
                    <div class="modal-content">
                     
                        <div class="modal-body">
                            <div class="box">
                                <div class="content">
                                 
                                    <div class="social">







                                      





                                    <%--<img src="../Images/logo.png"  alt="Sanjeev Group" />--%>
                                        
                                        <img src="assets/img/Group_logo.png" width="40px"  alt="Sanjeev Group" /><br />
                                         <asp:Label ID="lblApplicationName" Font-Size="Large" Font-Bold="true" runat="server" Text="Performance Analyser"></asp:Label>
                                    </div>
                                    <div class="error">
                                        
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="login" />
                                        <asp:Label ID="lblError" runat="server" Text="Invalid user name or password..." Visible="false" Font-Bold="true" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div class="form loginBox">
                                        <asp:TextBox ID="txtUsername" class="form-control" runat="server" placeholder="Email@sanjeevgroup.com" ValidationGroup="login" > </asp:TextBox>  <label></label>
                                        <asp:Label  Text="@sanjeevgroup.com" runat="server" ForeColor="Red" ></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername" Display="Dynamic" ErrorMessage="Enter Valid Email" ForeColor="#FF3300" ValidationGroup="login">*</asp:RequiredFieldValidator>
                                      
                                        <asp:TextBox ID="txtPassword" runat="server" class="form-control" type="password"   placeholder="Password" ValidationGroup="login"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Valid Password" Display="Dynamic" ControlToValidate="txtPassword" ForeColor="#FF3300" ValidationGroup="login">*</asp:RequiredFieldValidator>
                                        <asp:Button ID="LoginButton" runat="server" Text="Login" class="btn btn-default btn-login" ValidationGroup="login" OnClick="LoginButton_Click" />
                                         
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="forgot login-footer">
                                <span>
                                    <a href="javascript: showRegisterForm();"></a>
                                    <asp:Label ID="lblBuildId" runat="server" Text="Label"></asp:Label></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <telerik:RadWindow ID="RW_Confirm" Width="400px"  runat="server" VisibleStatusbar="false" Height="250px" Behaviors="None" Title="Login In With">
              <ContentTemplate>
                <p style="text-align: center; font-family: Bookman Old Style; font-style: italic; font-size: 40px">
                   Login Through?
                </p>
                <br />
                <table width="80%"  align="center" style="background-color: #FFF">
                  
                    <tr>
                        <td></td>
                       
                    </tr>
                    <tr align="center" >
                        <td style="padding-top:20px">
                            <telerik:RadButton ID="btnHR"  BackColor="Green" runat="server" Text="HR" Height="30px" Font-Size="20px"  OnClick="btnHR_Click"  />
                          
                        </td>
                        <td style="padding-top:20px">
                            <telerik:RadButton ID="btnAdmin"  runat="server" OnClick="btnAdmin_Click1" Height="30px" Text="Admin" Font-Size="20px" />
                          
                        </td>
                    </tr>
                   
                </table>
            </ContentTemplate>
        </telerik:RadWindow>
        <script type="text/javascript">
       


        function RedirectToOffice() {
            
                debugger;
                var OfficeRedirectUrl = "  <%=ConfigurationManager.AppSettings["ida:RedirectUri"]%>";
                var Office365ClientId = "<%=ConfigurationManager.AppSettings["ida:AppId"]%>";
                var OfficeUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?response_type=id_token+token&client_id=" + Office365ClientId + "&redirect_uri=" + OfficeRedirectUrl + "&scope=openid+https%3A%2F%2Foutlook.office.com%2Fmail.read&nonce=13c9217a-d565-fb9e-d66d-d5ad18fa6f0b&response_mode=form_post"; window.open(OfficeUrl, "_self");
            }
    </script>
    </form>
</body>
   
</html>
 
