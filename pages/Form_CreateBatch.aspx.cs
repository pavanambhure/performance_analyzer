﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class pages_Form_CreateBatch : System.Web.UI.Page
{
    string uploadedCsvFileName = string.Empty;

    string id = string.Empty;
    DataTable dtCsv = new DataTable();

    String[] month;

    Dictionary<string, string> dict_User = new Dictionary<string, string>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]).Equals(""))
        {
            Response.Redirect("Login.aspx");
        }


        month = new string[12] { "JAN", "FEB", "APR", "MAR", "MAY","JUN","JUL","AUG","SEPT","OCT","NOV","DEC" };


        if (!IsPostBack)
        {

            fnPopulateDepartment();
          //  fnPopulatePosition();
            fnPopulateLocation();
            fnPopulatePositionType();
           
        }
    }



    private void fnPopulatePositionType()
    {
        try
        {
            string query = "select test_Id,Title from tbl_ExamTestId where isActive='True'";
            DataTable dtPos = DBUtils.SQLSelect(new SqlCommand(query));
            //rddlPositionType.DataTextField = "Title";
            //rddlPositionType.DataValueField = "test_Id";
            //rddlPositionType.DataSource = dtPos;
            //rddlPositionType.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    protected void fnPopulateDepartment()
    {
        try
        {
            string query = "select * from tbl_Department_Master where isActive='True'";
            DataTable dtDept = DBUtils.SQLSelect(new SqlCommand(query));
            rddlDept.DataTextField = "Department_Name";
            rddlDept.DataValueField = "Department_Id";
            rddlDept.DataSource = dtDept;
            rddlDept.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    protected void fnPopulatePosition()
    {
        try
        {
            string query = "select * from tbl_Position_Master where isActive='True'";
            DataTable dtPos = DBUtils.SQLSelect(new SqlCommand(query));
            rddlPosition.DataTextField = "position_Name";
            rddlPosition.DataValueField = "position_Id";
            rddlPosition.DataSource = dtPos;
            rddlPosition.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    protected void fnPopulateLocation()
    {
        try
        {
            string query = "select * from tbl_Location_Master";
            DataTable dtLoc = DBUtils.SQLSelect(new SqlCommand(query));
            rddlLocation.DataTextField = "location_Name";
            rddlLocation.DataValueField = "location_Id";
            rddlLocation.DataSource = dtLoc;
            rddlLocation.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = ReadCsvFile();

            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            }
        catch (Exception ex)
        {
            lblerror.Text = ex.Message;
        }
    }
    public DataTable ReadCsvFile()
    {
        lblerror.Text = "";

        string Fulltext;
        if (FileUpload1.HasFile)
        {
            dict_User.Clear();

            string user = Convert.ToString(Session[PublicMethods.ConstUserEmail]).Substring(0, Convert.ToString(Session[PublicMethods.ConstUserEmail]).IndexOf("@"));
            string FileSaveWithPath = Server.MapPath("~\\CSV\\Onboarder_" + user + "_" + System.DateTime.Now.ToString("ddMMMyyyy_hhmm") + ".csv");
            Session["uploadedCsvFileName"] = "Onboarder_" + user + "_" + System.DateTime.Now.ToString("ddMMMyyyy_hhmm") + ".csv";
            FileUpload1.SaveAs(FileSaveWithPath);
            using (StreamReader sr = new StreamReader(FileSaveWithPath))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    if (rowValues.Count() != 5) {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Only 5  column allowd in file,For reference Please download sample excel');", true);
                                        break;
                                    }

                                    if (rowValues[0].ToString().ToUpper().Trim() != "NAME") {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First column must be Name,For reference Please download sample excel');", true);
                                        break;
                                    }
                                    if (rowValues[1].ToString().ToUpper().Trim() != "EMAIL")
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Second column must be Email,For reference Please download sample excel');", true);
                                        break;

                                    }
                                    if (rowValues[2].ToString().ToUpper().Trim() != "BIRTHDATE")
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Third column must be BIRTHDATE,For reference Please download sample excel');", true);
                                        break;
                                    }
                                    if (rowValues[3].ToString().ToUpper().Trim() != "MOBILE NO")
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fourth column must be MOBILE NO,For reference Please download sample excel');", true);
                                        break;
                                    }
                                    if (rowValues[4].ToString().ToUpper().Trim() != "EXPERIENCE")
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fifth column must be EXPERIENCE,For reference Please download sample excel');", true);
                                        break;
                                    }

                                    dtCsv.Columns.Add(rowValues[j]); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    if (!String.IsNullOrEmpty(rowValues[k].ToString()))
                                    {
                                        
                                        
                                           
                                            dr[k] = rowValues[k].ToString();
                                        
                                       

                                       
                                    }
                                    else {
                                        break;
                                    }

                                }


                             bool emptyFound=   fnCheckDatarowHasNullValue(dr);

                                if (!emptyFound)
                                {
                                    dtCsv.Rows.Add(dr);//add other rows  
                                }
                                else {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('File should not contain blank column.');", true);

                                    break;
                                }
                                
                            }
                        }
                    }
                }
            }
        }


       

        if (dtCsv.Rows.Count > 0)
        {
            foreach (DataRow item in dtCsv.Rows)
            {
              

                if (dict_User.ContainsKey(Convert.ToString(item["EMAIL"])))
                {
                    string message1 = "Duplicate Email id found  " + Convert.ToString(item["EMAIL"]);
                    string url1 = "Form_CreateBatch.aspx";
                    string script1 = "window.onload = function(){ alert('";
                    script1 += message1;
                    script1 += "');";
                    script1 += "window.location = '";
                    script1 += url1;
                    script1 += "'; }";
                    ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script1, true);
                }
                else {
                    
                   
                   
                    dict_User.Add(Convert.ToString(item["EMAIL"]), "TRUE");
                }
            }
        }
        return dtCsv;


    }

    private bool fnCheckDatarowHasNullValue(DataRow dr)
    {
        if (String.IsNullOrEmpty(Convert.ToString(dr[0])) || String.IsNullOrEmpty(Convert.ToString(dr[1])) || String.IsNullOrEmpty(Convert.ToString(dr[2])) || String.IsNullOrEmpty(Convert.ToString(dr[3])) || String.IsNullOrEmpty(Convert.ToString(dr[4])) ) {
            return true;
        }
        else {
            return false;
                
                }

    }

    protected void btnScheduleInterview_Click(object sender, EventArgs e)
    {
        if (GridView1.Rows.Count > 0)
        {
            int i = 0;

            foreach (GridViewRow row in GridView1.Rows)
            {

                DropDownList ddlPosition = (DropDownList)row.FindControl("ddlPosition");

                String examid = ddlPosition.SelectedItem.Value;
                String Position = ddlPosition.SelectedItem.Text;
                String Name = row.Cells[1].Text;
                if (examid.ToUpper().Trim().Equals("PLEASE SELECT") || Position.ToUpper().Trim().Equals("PLEASE SELECT")) {
                    i++;
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Select Position For : "+ Name + "');", true);
                    return;
                }
                

            }

            if (i == 0)
            {

                string script = "function f(){$find(\"" + RW_Approve.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            }
        }
        else
        {


            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Data found to create batch');", true);
            return;
        }



    }

    protected void btnDownloadSampleExcel_Click(object sender, EventArgs e)
    {
        try
        {
            string path = "~/CSV/Sample/Onboarder_Candidate.csv";

            if (System.IO.File.Exists(Server.MapPath(path)))
            {



                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                string filePath = path;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=onBoarder_sample.csv");
                byte[] data = req.DownloadData(Server.MapPath(filePath));
                response.BinaryWrite(data);
                response.End();
            }
            else
            {
                lblerror.Text = "File not found";
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }







   

    protected void btnYes_Click(object sender, EventArgs e)
    {
        string batchNo = DateTime.Now.ToString("ddMMyyyhhmm");
        try
        {

           
            string qry_insertBatchDetails = "INSERT INTO [tbl_batch_information]([BatchNo],[Date],[createdBy],[created_at],[department_id],[position_id],[location_id],[uploaded_file_name],remark) VALUES ('" + batchNo + "','" + DateTime.Now.ToString("dd/MMM/yyyy hh:mm") + "','" + Convert.ToString(Session[PublicMethods.ConstUserEmail]) + "','" + DateTime.Now.ToString() + "','" +Convert.ToString(rddlDept.SelectedValue)+ "','8','" + Convert.ToString(rddlLocation.SelectedValue) + "','"+Convert.ToString(Session["uploadedCsvFileName"]) + "','"+ txtRemark.Text.Replace("'", "''").ToString() + "')";


            int i = DBUtils.ExecuteSQLCommand(new SqlCommand(qry_insertBatchDetails));


            if (i > 0)
            {
                foreach (GridViewRow row in GridView1.Rows)
                {
                    // here you'll get all rows with RowType=DataRow
                    // others like Header are omitted in a foreach

                    DropDownList ddlPosition = (DropDownList)row.FindControl("ddlPosition");

                    String examid = ddlPosition.SelectedItem.Value;
                    String Position_type = ddlPosition.SelectedItem.Text.Trim();

                    String Name = row.Cells[1].Text;
                    String email = row.Cells[2].Text;
                    string birthdate = row.Cells[3].Text;
                    
                    //DateTime date_birth = DateTime.ParseExact(birthdate, "dd/MMM/yyyy", null);


                    var today =  DateTime.Today;
                    // Calculate the age.
                    var age = 0;
                    try
                    {

                   
                     age = today.Year - Convert.ToDateTime(birthdate).Year;
                    if (Convert.ToDateTime(birthdate).Date > today.AddYears(-age)) age--;

                    }
                    catch (Exception)
                    {
                        


                        string message1 = " Month name should be Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec,Please create batch again";
                        string url1 = "Form_CreateBatch.aspx";
                        string script1 = "window.onload = function(){ alert('";
                        script1 += message1;
                        script1 += "');";
                        script1 += "window.location = '";
                        script1 += url1;
                        script1 += "'; }";
                        ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script1, true);
                        

                        string qry = "DELETE FROM [tbl_batch_information] WHERE BatchNo='" + batchNo + "'";
                             DBUtils.ExecuteSQLCommand(new SqlCommand(qry));
                        return;

                        

                    }

                    String mobile = row.Cells[4].Text;
                    String experience = row.Cells[5].Text;


                   







                   
                    string q = "INSERT INTO [tbl_batch_candidate_details] ([BatchNo],[Candidate_Name],[Candidate_Email],[BirthDate],[age],[Mobile],[Experience],[Exam_Status],position_type,position_examId) VALUES ('" + batchNo + "','"+ Name + "','"+ email + "','"+ vtSqlConnection.vtWeb.Date0.Format_DD_MON_YYYY__HH_MI_SS(Convert.ToDateTime(birthdate))  + "','"+age+"','"+mobile+"','"+experience+"','"+PublicMethods.examStatus_Created + "','" + Position_type + "','" + examid + "')";


                    int res = DBUtils.ExecuteSQLCommand(new SqlCommand(q));

                    if (res <= 0) {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Error while inserting for candidate + "+ Name + "');", true);
                       
                    }

                }



                string message = " Data saved Successfully...";
                string url = "ExamDashboard.aspx";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "');";
                script += "window.location = '";
                script += url;
                script += "'; }";
                ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);




            }
            else {

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Incorrect Data');", true);
                return;
            }

           

        }
        catch (Exception ex)
        {
            string qry = "DELETE FROM [tbl_batch_information] WHERE BatchNo='" + batchNo + "'";
            DBUtils.ExecuteSQLCommand(new SqlCommand(qry));
            throw ;
        }

    }

    protected void cancelClick_Click(object sender, EventArgs e)
    {
        return;
    }

    protected void btnDone_Click(object sender, EventArgs e)
    {
        Response.Redirect("ExamDashboard.aspx");
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Find the DropDownList in the Row
                DropDownList ddlPosition = (e.Row.FindControl("ddlPosition") as DropDownList);
                string query = "select test_Id,Title from tbl_ExamTestId where isActive='True'";
                DataTable dtPos = DBUtils.SQLSelect(new SqlCommand(query));
                ddlPosition.DataSource = dtPos;
                ddlPosition.DataTextField = "Title";
                ddlPosition.DataValueField = "test_Id";
                ddlPosition.DataBind();

                //Add Default Item in the DropDownList
                ddlPosition.Items.Insert(0, new ListItem("PLEASE SELECT"));
            }
            }
        catch (Exception ex)
        {

            throw ex;
        }
    }
}