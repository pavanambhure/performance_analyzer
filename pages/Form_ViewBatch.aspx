﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/examMaster.master" AutoEventWireup="true" CodeFile="Form_ViewBatch.aspx.cs" Inherits="pages_Form_ViewBatch" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>View Batch</title>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BodyStart" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
      <telerik:RadWindowManager ID="rmw1" runat="server">
    </telerik:RadWindowManager>








    <script type="text/javascript">
       
        function MyFunction()
        {
            $('#loading').show();
        }
        function MyFunctionHide() {
            $('#loading').Hide();
        }
    </script>
   



    <style type="text/css">
  .hiddencol
  {
    display: none;
  }
</style>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body1" runat="Server">
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body2" runat="Server">
    <div class="content">
                    <div class="container-fluid">


                          <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="content">
                           <table class="table table-bordered">
                               <tr>
                                   <td>Batch </td>
                                    <td>  <asp:Label Font-Bold="true" ID="lbl_BatchName"  Style="font-size:large" runat="server" ></asp:Label>

                                       (  <asp:Label Font-Bold="true" ID="lblBatchStatus"  Style="font-size:large" runat="server" ></asp:Label>)

                                    </td>
                                    <td>Total Candidate</td>
                                    <td><asp:Label Font-Bold="true" ID="lblTotalCandidate"  Style="font-size:large" runat="server" ></asp:Label></td>
                                     <td>Exam Link Sent to</td>
                                    <td><asp:Label Font-Bold="true" ID="lblExamLinkSentTo"  Style="font-size:large" runat="server" ></asp:Label></td>
                               </tr>

                               <tr>
                                    
                                   <td>Department</td>
                                    <td><asp:Label Font-Bold="true" ID="lblDepartment"  Style="font-size:large" runat="server" ></asp:Label></td>
                                    <td>Location</td>
                                    <td><asp:Label Font-Bold="true" ID="lbllocation"  Style="font-size:large" runat="server" ></asp:Label></td>
                                    <td>Created By</td>
                                    <td><asp:Label Font-Bold="true" ID="lblCreatedBy"  Style="font-size:large" runat="server" ></asp:Label></td>
                                   
                               </tr>
                               <tr>
                                  
                                    <td>Created AT</td>
                                    <td><asp:Label Font-Bold="true" ID="lblCreatedAt"  Style="font-size:large" runat="server" ></asp:Label></td>
                                    <td>Remark</td>
                                    <td colspan="3"><asp:Label Font-Bold="true" ID="lblRemark"  Style="font-size:large" runat="server" ></asp:Label></td>
                                   
                               </tr>
                           </table>
                            </div>
                        </div>
                    </div>
                </div>
</div>


                        <div class="row" style="margin-top: 0px !important;">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">View Batch</h4>
                                        <table>
                                            <tr>
                                                <td> <button style="pointer-events: none;
cursor: default;" class="btn btn-block" >Pending</button></td>
                                                  <td><button  class="btn" style="background:lightBlue;pointer-events: none;
cursor: default;" >Link Sent</button></td>
                                                  <td><button class="btn" style="background:lightGreen;pointer-events: none;
cursor: default;" >Completed</button></td>
                                            </tr>
                                        </table>
                                     
                                         
                                         
                                    </div>

                                 <div>


                                     


                                                  
                                                <div class="content table-responsive table-full-width" style="overflow:scroll">
                                            <asp:GridView CssClass="table table-bordered" ID="GridView1" AutoGenerateColumns="false" runat="server" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound" Font-Size="Large" >
                              <Columns>
                                <asp:TemplateField>
            <ItemTemplate>
                <asp:Button Text="Send" CssClass="btn btn-fill" runat="server" CommandName="Select" CommandArgument="<%# Container.DataItemIndex %>"  OnClientClick="somefunction();"  Visible='<%# Eval("Exam_Status").ToString() == PublicMethods.examStatus_Created ? true : false %>' />
            </ItemTemplate>

        </asp:TemplateField>
                                 

                                   <asp:TemplateField>
            <ItemTemplate>
                <asp:Button Text="View"   CssClass="btn btn-fill" runat="server" CommandName="view_details" CommandArgument='<%# Eval("Candidate_Email").ToString() %>'  Visible='<%# Eval("Exam_Status").ToString() == "COMPLETED" || Eval("Exam_Status").ToString() == "LINK SENT" ? true : false %>' />
            </ItemTemplate>

        </asp:TemplateField>
                                   <asp:BoundField DataField="position_type" HeaderText="Position"  />

                                   <asp:BoundField DataField="Candidate_Name" HeaderText="Candidate Name"  />


                              
                                   <asp:BoundField DataField="Candidate_Email" 
HeaderText="Email" SortExpression="Candidate_Email" />
                                   <asp:BoundField DataField="BirthDate" HeaderText="Birth Date" />
                                   <asp:BoundField DataField="age" HeaderText="Age" />
                                    <asp:BoundField DataField="type" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="type" />
                                    <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                                    <asp:BoundField DataField="Experience" HeaderText="Experience" />
                                    <asp:BoundField DataField="Exam_Status" HeaderText="Exam Status" />
                                    <asp:BoundField DataField="id" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="id" />
                                   <asp:BoundField DataField="position_examId" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="position_examId" />
                                    


                                    </Columns>
                          </asp:GridView>                                     </div>


                                              




                                         


                                        


                                         
                                    















                                          
                                    

                                 </div>

                                    
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
    

              
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer" runat="Server">
 <div class="loading" align="center">
<img src="../pages/images/ajax-loader.gif" alt="Loading" /><br /><br /><br /><h3>Sending emails,Do not close or refresh the page. please wait...</h3>
</div>
              <script type="text/javascript">
                  function ShowProgress() {
                      setTimeout(function () {
                          var modal = $('<div />');
                          modal.addClass("modal");
                          $('body').append(modal);
                          var loading = $(".loading");
                          loading.show();
                          var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                          var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                          loading.css({ top: top, left: left });
                      }, 200);
                  }
                  $('form').live("submit", function () {
                      ShowProgress();
                  });
                  function ShowMaskLayer() {
                      var sWidth, sHeight;
                      sWidth = window.screen.availWidth;
                      if (window.screen.availHeight > document.body.scrollHeight) {
                          sHeight = window.screen.availHeight;
                      } else {
                          sHeight = document.body.scrollHeight;
                      }
                      var maskObj = document.createElement("div");
                      maskObj.setAttribute('id', 'BigDiv');
                      maskObj.style.position = "absolute";
                      maskObj.style.top = "0";
                      maskObj.style.left = "0";
                      maskObj.style.background = "#777";
                      maskObj.style.filter = "Alpha(opacity=30);";
                      maskObj.style.opacity = "0.3";
                      maskObj.style.width = sWidth + 200  + "px";
                      maskObj.style.height = sHeight + "px";
                      maskObj.style.zIndex = "10000";
                      document.body.appendChild(maskObj);
                  }
                  function somefunction() {
                      debugger;
                   
                          ShowProgress();
                          ShowMaskLayer();
                     
                  }
</script>

  <style type="text/css">
    .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        z-index: 999;
    }
</style>
    

    
</asp:Content>



