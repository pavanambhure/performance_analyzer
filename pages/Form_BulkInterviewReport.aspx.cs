﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_Form_BulkInterviewReport : System.Web.UI.Page
{
    string from = string.Empty;
    string to = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]).Equals(""))
        {
            Response.Redirect("Login.aspx");
        }
        if (!IsPostBack)
        {
            dtpFromDate.SelectedDate = DateTime.Now.AddDays(-30);
            dtpToDate.SelectedDate = DateTime.Now;
            from = dtpFromDate.SelectedDate.ToString();
            to = dtpToDate.SelectedDate.ToString();
            fnLoadData(true);
           

        }
        else
        {
            from = dtpFromDate.SelectedDate.ToString();
            to = dtpToDate.SelectedDate.ToString();

        }
    }

    protected void rgInterView_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            fnLoadData(false);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void fnLoadData(Boolean DoRebind)
    {
        try
        {







            double exp = Convert.ToDouble(ConfigurationManager.AppSettings["fresherExpUpto"].ToString());
           


            string query = "SELECT Id_Exam as User_Id,[User_Name],[User_Email],CONVERT(VARCHAR(15),tbl_JombayExam_NewInterviewMaster.Birthdate, 106) AS Birthdate,[age],(select Department_Name from [tbl_Department_Master] where Department_Id=[tbl_JombayExam_NewInterviewMaster].department_Id) as Department_Name,position_type,(select Location_Name from tbl_Location_Master where location_Id=[tbl_JombayExam_NewInterviewMaster].location_Id) as Location_Name,[remark],[jombayLink],SUBSTRING([tbl_JombayExam_NewInterviewMaster].arranged_By, 0, CHARINDEX('@', [tbl_JombayExam_NewInterviewMaster].arranged_By)) AS arranged_By,[created_At],[contact],[type],[Experience],Case when [isAccepted] IS NULL then 'Not Received' else [isAccepted] end as [isAccepted],BatchNo FROM [tbl_JombayExam_NewInterviewMaster]  where tbl_JombayExam_NewInterviewMaster.created_At between '" + from + "' and '" + to + "' and BatchNo Is NOT NULL ORDER BY [tbl_JombayExam_NewInterviewMaster].created_At DESC";


            DataTable dt = DBUtils.SQLSelect(new SqlCommand(query));







            rgInterView.DataSource = dt;
            if (DoRebind == true)
                rgInterView.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnFilterApply_Click(object sender, EventArgs e)
    {
        from = dtpFromDate.SelectedDate.ToString();
        to = dtpToDate.SelectedDate.ToString();
        fnLoadData(true);
    }
}