﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/examMaster.master" AutoEventWireup="true" CodeFile="Form_CreateBatch.aspx.cs" Inherits="pages_Form_CreateBatch" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Create Batch</title>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BodyStart" runat="Server">
  <script type="text/javascript">
    function OnClientClicked(sender, args) {
        $('#loading').show();
        var window = $find('<%=btnScheduleInterview.ClientID %>');
        RW_Approve.close();
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body1" runat="Server">

    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body2" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
     <telerik:RadWindowManager ID="rmw1" runat="server">
    </telerik:RadWindowManager>






      <telerik:RadWindow ID="RW_Approve" CssClass="RadWindow_Top" runat="server"  Height="180px" Skin="Bootstrap" Modal="true" VisibleStatusbar="false" Behaviors="Close">
                        <ContentTemplate>
                            <p style="text-align: center; font-family: Bookman Old Style; font-style: italic; font-size: medium">
                              Are you sure to create this batch?
                            </p>
                            <br />
                            <table width="80%" align="center" style="background-color: #FFF">

                                <tr id="Tr1" align="center" runat="server" style="background-color: #FFF">
                                    <td>
                                        <asp:Button ID="btnYes" runat="server" Text="Yes"  Font-Bold="true" OnClick="btnYes_Click"  Width="30%"  OnClientClick="somefunction();" />
                                    <asp:Button ID="cancelClick" runat="server" Text="No" OnClientClick="somefunction();" Font-Bold="true" OnClick="cancelClick_Click" Width="30%" />
                                       
                                    </td>
                                </tr>
                               
                            </table>
                        </ContentTemplate>
                    </telerik:RadWindow>






    <div class="content">
                    <div class="container-fluid">
                         <div class="row">
                   <div class="col-lg-12">

                         <div class="card">
                       



                        <div class="content" style="overflow:scroll">
                              <label>Office Details</label>
                            
                      <table class="table table-bordered">
 
  <tr>

     <%-- <td>Position Type</td>
      <td>
          <telerik:RadDropDownList ID="rddlPositionType" runat="server"   DefaultMessage="Select Position Type" Font-Bold="false" Font-Size="Large" ValidationGroup="validationSave"  Skin="Bootstrap" Width="80%" ></telerik:RadDropDownList>
      <asp:RequiredFieldValidator ID="rfvPositionType" runat="server" ControlToValidate="rddlPositionType" ForeColor="Red" ErrorMessage="* Required" Display="Dynamic" ValidationGroup="validationSave"></asp:RequiredFieldValidator>
      </td>--%>


    <td>Department</td>
    <td>
<telerik:RadDropDownList ID="rddlDept" runat="server"   DefaultMessage="Select Department" Font-Bold="false" Font-Size="Large" ValidationGroup="validationSave"  Skin="Bootstrap" Width="80%"></telerik:RadDropDownList>
            <asp:RequiredFieldValidator ID="rfvDept" runat="server" ControlToValidate="rddlDept" ForeColor="Red" ErrorMessage="* Required" Display="Dynamic" ValidationGroup="validationSave"></asp:RequiredFieldValidator>
</td>

       <td>Location</td>
    <td><telerik:RadDropDownList ID="rddlLocation" runat="server"  DefaultMessage="Select Location" Font-Bold="false" Font-Size="Large" ValidationGroup="validationSave" Skin="Bootstrap" Width="80%"></telerik:RadDropDownList>
          <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="rddlLocation" ForeColor="Red" ErrorMessage="* Required" Display="Dynamic" ValidationGroup="validationSave"></asp:RequiredFieldValidator>
    </td>


     <td runat="server" visible="false">Position</td>
    <td runat="server" visible="false"><telerik:RadDropDownList ID="rddlPosition" runat="server"  Font-Bold="false" Font-Size="Large"   ValidationGroup="validationSave" Skin="Bootstrap" Width="80%"></telerik:RadDropDownList>
         
    </td>
  </tr>
  
  <tr>
   
    <td>Remark</td>
    <td colspan="3"><telerik:RadTextBox ID="txtRemark" Width="100%"  placeholder="Remark" RenderMode="Lightweight" TextMode="MultiLine" runat="server"  Font-Bold="false" Font-Size="Large" Skin="Bootstrap" ></telerik:RadTextBox></td>
  </tr>
 
  
</table>

                           
                             

                             
                        </div>
                    </div>


                  <div class="card">
                       



                        <div class="content">
                              <label>Upload Excel</label>
                            <div class="form-inline">
    <div class="form-group">
     
        <asp:FileUpload ID="FileUpload1" accept=".csv" runat="server"  CssClass="form-control-file" />   
    </div>
    <div class="form-group">
     
       <asp:Button ID="btnUpload" runat="server" Text="Upload" ValidationGroup="validation" OnClick="btnUpload_Click"  CssClass="btn btn-info" BackColor="ForestGreen" ForeColor="White"/>
    </div>
    <div class="checkbox">
       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please upload CSV file." ControlToValidate="FileUpload1" ForeColor="Red" ValidationGroup="validation">  
                        </asp:RequiredFieldValidator> 
    </div>
                                 <div class="form-group">
     
       <asp:Button ID="btnDownloadSampleExcel" runat="server" Text="Download Sample File" OnClick="btnDownloadSampleExcel_Click" BackColor="RoyalBlue" ForeColor="White"  CssClass="btn btn-info"/>
                                     <label>Please upload same file format as given in sample CSV.</label>
    </div>
    
  </div>

                           
                             

                             
                        </div>
                    </div>

                         <div class="card">
                       
                        <div class="content" style="overflow:scroll;height:300px">
                            <label>Excel Data</label>
                           
                          <asp:GridView CssClass="table table-bordered" ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound" >
                              <Columns>
                                <asp:TemplateField HeaderText="Position" HeaderStyle-Width="20%">
  <ItemTemplate>
       <asp:DropDownList ID="ddlPosition" Font-Bold="true" style="background:lightgray" CssClass="form-control" runat="server">
                </asp:DropDownList>
   
  </ItemTemplate> 
</asp:TemplateField>
                                    </Columns>
                          </asp:GridView>  
                            <asp:Label ID="lblerror" runat="server" />  
                        </div>
                    </div>


                         <div class="card">
                       



                        <div class="content">
                              
                          

                           <center>
                               
                               
                               
                               <asp:Button  ID="btnScheduleInterview" runat="server" Text="Create Batch" ValidationGroup="validationSave" OnClick="btnScheduleInterview_Click" BackColor="OrangeRed" ForeColor="White"  CssClass="btn"/></center>
                             

                             
                        </div>
                    </div>

                       </div>
                 </div>



                      
                    </div>
                </div>

     <div class="loading" align="center">
<img src="../pages/images/ajax-loader.gif" alt="Loading" /><br /><br /><br /><h3>Saving Data,Do not close or refresh the page. please wait...</h3>
</div>
              <script type="text/javascript">
                  function ShowProgress() {
                      setTimeout(function () {
                          var modal = $('<div />');
                          modal.addClass("modal");
                          $('body').append(modal);
                          var loading = $(".loading");
                          loading.show();
                          var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                          var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                          loading.css({ top: top, left: left });
                      }, 200);
                  }
                  $('form').live("submit", function () {
                      ShowProgress();
                  });
                  function ShowMaskLayer() {
                      var sWidth, sHeight;
                      sWidth = window.screen.availWidth;
                      if (window.screen.availHeight > document.body.scrollHeight) {
                          sHeight = window.screen.availHeight;
                      } else {
                          sHeight = document.body.scrollHeight;
                      }
                      var maskObj = document.createElement("div");
                      maskObj.setAttribute('id', 'BigDiv');
                      maskObj.style.position = "absolute";
                      maskObj.style.top = "0";
                      maskObj.style.left = "0";
                      maskObj.style.background = "#777";
                      maskObj.style.filter = "Alpha(opacity=30);";
                      maskObj.style.opacity = "0.3";
                      maskObj.style.width = sWidth + 200  + "px";
                      maskObj.style.height = sHeight + "px";
                      maskObj.style.zIndex = "10000";
                      document.body.appendChild(maskObj);
                  }
                  function somefunction() {
                      debugger;
                      if (Page_IsValid) {
                          ShowProgress();
                          ShowMaskLayer();
                      }
                  }
</script>

  <style type="text/css">
    .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        z-index: 999;
    }
</style>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer" runat="Server">


    

  
</asp:Content>



