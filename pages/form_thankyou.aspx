﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="form_thankyou.aspx.cs" Inherits="pages_form_thankyou" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

   <head>
      <title>Feedback</title>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
      <meta name='viewport' content='width=device-width, initial-scale=1'>
      <meta http-equiv='X-UA-Compatible' content='IE=edge' />
      <style type='text/css'>/* CLIENT-SPECIFIC STYLES /body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }img { -ms-interpolation-mode: bicubic; }/ RESET STYLES /img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }table { border-collapse: collapse !important; }body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }/ iOS BLUE LINKS /a[x-apple-data-detectors] {    color: inherit !important;    text-decoration: none !important;    font-size: inherit !important;    font-family: inherit !important;    font-weight: inherit !important;    line-height: inherit !important;}/ MEDIA QUERIES /@media screen and (max-width: 480px) {    .mobile-hide {display: none !important;    }    .mobile-center {        text-align: center !important;    }}/ ANDROID CENTER FIX */div[style*='margin: 16px 0;'] { margin: 0 !important; }</style>
         <style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>

       <form runat="server">
   <body style='margin: 0 !important; padding: 0 !important; background-color: #eeeeee;' bgcolor='#eeeeee'>
      <!-- HIDDEN PREHEADER TEXT -->
      <div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Open Sans, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>Exam Link</div>
      <table border='0' cellpadding='0' cellspacing='0' width='100%'>
         <tr>
            <td align='center' style='background-color: #eeeeee;' bgcolor='#eeeeee'>
               <!--[if (gte mso 9)|(IE)]>        
               <table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>
                  <tr>
                     <td align='center' valign='top' width='600'>
                        <![endif]-->        
                        <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width:600px;'>
                           <tr>
                              <td align='center' valign='top' style='font-size:0; padding: 35px;' bgcolor='#4CAF50'>
                                               
                              </td>
                           </tr>
                           <tr>
                              <td align='center' style='padding: 35px 35px 20px 35px; background-color: #ffffff;' bgcolor='#ffffff'>
                                               
                                          <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width:600px;'>
                                             <tr>
                                                <td align='center' style='font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;'>
                                                   <img src='http://sanjeevauto.com/wp-content/uploads/2017/05/logo.png'  /><br>                            
                                                  
                                                </td>
                                             </tr>
                                           
                                             <tr>
                                                <td align='left' style='padding-top: 20px;'>
                                                   <table cellspacing='0' cellpadding='0' border='0' width='100%'>
                                                      <tr>
                                                         <td width='75%' align='center' bgcolor='#eeeeee' style='font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 800; line-height: 24px; padding: 10px;' colspan='2' >                                         
                                                             <asp:Label id="status1" runat="server" Text="Status"></asp:Label>
                                                      
                             <h4>Thank you for your feedback
                                 <br />
                                  <asp:Label  forecolor="red" runat="server" id="lbl1" Text=""></asp:Label>
                             </h4>
                                                               </td>
                                                      </tr>
                                                   
                                                   <tr>

                                                       <table cellspacing='0' cellpadding='0' border='0' width='100%'  id="customers">
                                                     
             <tr>
    <td>Candidate Name</td>
    <td style="font-weight: bold;">  <asp:Label   runat="server" id="lblName" Text=""></asp:Label></td>
 
  </tr>                                    
  <tr>
    <td>Candidate Email</td>
    <td style="font-weight: bold;">  <asp:Label   runat="server" id="lblEmail" Text=""></asp:Label></td>
 
  </tr>
  <tr>
    <td>Birthdate</td>
    <td  style="font-weight: bold;"> <asp:Label   runat="server" id="lblBirth" Text=""></asp:Label></td>
 
  </tr> <tr>
    <td>Age</td>
    <td  style="font-weight: bold;"> <asp:Label  runat="server" id="lblAge" Text=""></asp:Label></td>
 
  </tr> <tr>
    <td>Location</td>
    <td  style="font-weight: bold;"> <asp:Label   runat="server" id="lblLocation" Text=""></asp:Label></td>
 
  </tr> <tr>
    <td>Position</td>
    <td  style="font-weight: bold;"> <asp:Label   runat="server" id="lblPosition" Text=""></asp:Label></td>
 
  </tr> <tr>
    <td>Department</td>
    <td  style="font-weight: bold;"> <asp:Label   runat="server" id="lblDeparment" Text=""></asp:Label></td>
 
  </tr> <tr>
    <td>Experience</td>
    <td  style="font-weight: bold;"> <asp:Label   runat="server" id="lblExperience" Text=""></asp:Label></td>
 
  </tr> <tr>
    <td>Arranged By</td>
    <td  style="font-weight: bold;"> <asp:Label  runat="server" id="lblArrangedBy" Text=""></asp:Label></td>
 
  </tr> <tr>
    <td>Created At</td>
    <td  style="font-weight: bold;"> <asp:Label   runat="server" id="lblCreatedAt" Text=""></asp:Label></td>
 
  </tr> 
                                                           <tr >
    <td colspan="2" style="text-align: center;font-size: x-large;"><asp:Label runat="server" id="lblUrl" Text="" visible="false"></asp:Label>

        <a id="reportLink" runat="server" >View report</a>

    </td>
   
  </tr>
                                                   
                                                   </table>
                                                </td>
                                             </tr>
                                           
                                          </table>


                                                   </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                             
                                          </table>
                                                  
                              </td>
                           </tr>
                          
                          
                        
                        </table>
                        <!--[if (gte mso 9)|(IE)]>        
                     </td>
                  </tr>
               </table>
               <![endif]-->        
            </td>
         </tr>
      </table>
   </body>
           </form>
</html>


