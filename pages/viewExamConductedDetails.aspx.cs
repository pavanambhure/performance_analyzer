﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_viewExamConductedDetails : System.Web.UI.Page
{
    string userEmail = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]).Equals(""))
        {
            Response.Redirect("Login.aspx");
        }

        userEmail = Convert.ToString(Request.QueryString["email"]);

        fnLoadCandidateDetails(userEmail);
        fnLoadExamDetails(userEmail);
        fnLoadHrQue(userEmail);

    }

    private void fnLoadHrQue(string userEmail)
    {
        try
        {
            DataTable dt;
            string qry = "select TOP 1 [hr_questions] from [tbl_AssessmentTest_Data] where [Candidate_Email] ='"+ userEmail + "' order by inserted_at desc";

            string jsonData = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));

            string json = "[ " + jsonData + " ] ";

            if (!string.IsNullOrEmpty(jsonData))
            {
                DataTable tester = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
                //dt = (DataTable)JsonConvert.DeserializeObject(jsonData, (typeof(DataTable)));

                DataTable transposedData = GenerateTransposedTable(tester, "HR");

                hrSection.DataSource = transposedData;
                hrSection.DataBind();
            }
            else {

                hrSection.DataSource = null;
                hrSection.DataBind();
            }



        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    private void fnLoadExamDetails(string userEmail)
    {
        try
        {
            DataTable dt;



            string q = "SELECT  Replace(REPLACE(REPLACE(tool_statuses,'_',' '),'is',''),'completed','') as [Test],Case when  [result]  = 'True' then 'Completed' else 'Pending' end as  result FROM [tbl_ToolStatus_Master] where candidate_email='" + userEmail + "' order by rtc desc";
            DataTable dt_res = DBUtils.SQLSelect(new SqlCommand(q));

            if (dt_res.Rows.Count > 0)
            {
                examDetails.DataSource = dt_res;
                examDetails.DataBind();
            }
            else
            {

                string qry = "select TOP 1 tool_Status from [tbl_Incompleted_AssessmentTest_Data] where [Candidate_Email] ='" + userEmail + "' order by inserted_at desc";

                string jsonData = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));

                string json = "[ " + jsonData + " ] ";

                if (!string.IsNullOrEmpty(jsonData))
                {
                    DataTable tester = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                    DataTable dtClone = tester.Clone(); //just copy structure, no data
                    for (int i = 0; i < dtClone.Columns.Count; i++)
                    {
                        if (dtClone.Columns[i].DataType != typeof(string))
                            dtClone.Columns[i].DataType = typeof(string);
                    }

                    foreach (DataRow dr in tester.Rows)
                    {
                        dtClone.ImportRow(dr);
                    }

                    DataTable transposedData = GenerateTransposedTable(dtClone, "Exam");

                    DataTable dt_New = new DataTable();
                    dt_New.Clear();
                    dt_New.Columns.Add("Test");
                    dt_New.Columns.Add("Result");
                    foreach (DataRow dr in transposedData.Rows)
                    {
                        string res = string.Empty;
                        String testName = ConfigurationManager.AppSettings[Convert.ToString(dr["Test"])];


                        if (string.IsNullOrEmpty(testName))
                        {
                            testName = Convert.ToString(dr["Test"]).Replace("_", " ").Replace("Completed", "");
                        }


                        if (Convert.ToString(dr["Data"]).Equals("True"))
                        {
                            res = "Completed";
                        }
                        if (Convert.ToString(dr["Data"]).Equals("False"))
                        {

                            string qry_Result = "select count(*) from [tbl_AssessmentTest_Data] where [Candidate_Email] ='" + userEmail + "'";

                            string count = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry_Result));

                            if (Convert.ToInt32(count) > 0)
                            {
                                res = "Completed";
                            }
                            else
                            {
                                res = "Pending";
                            }

                        }

                        dt_New.Rows.Add(testName, res);
                    }


                    examDetails.DataSource = dt_New;
                    examDetails.DataBind();
                }
                else
                {
                    examDetails.DataSource = null;
                    examDetails.DataBind();
                }
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }

    private void fnLoadCandidateDetails(string userEmail)
    {
        try
        {
            string qry = " SELECT TOP 1 Id_Exam as User_Id,[User_Name],contact,[User_Email],CONVERT(VARCHAR(15),tbl_JombayExam_NewInterviewMaster.Birthdate, 106) AS Birthdate,[age],(select Department_Name from[tbl_Department_Master] where Department_Id =[tbl_JombayExam_NewInterviewMaster].department_Id) as Department_Name,position_type,(select Location_Name from tbl_Location_Master where location_Id=[tbl_JombayExam_NewInterviewMaster].location_Id) as Location_Name,[remark],[jombayLink],SUBSTRING([tbl_JombayExam_NewInterviewMaster].arranged_By, 0, CHARINDEX('@', [tbl_JombayExam_NewInterviewMaster].arranged_By)) AS arranged_By,[created_At],[contact],[type],[Experience],[isAccepted] FROM [tbl_JombayExam_NewInterviewMaster] where tbl_JombayExam_NewInterviewMaster.User_Email='" + userEmail + "'";

            DataTable dt = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));
            foreach (DataRow dr in dt.Rows)
            {
                lblCandidateEmail.Text = Convert.ToString(dr["User_Email"]);
                lblName.Text = Convert.ToString(dr["User_Name"]);
                lblExp.Text = Convert.ToString(dr["Experience"]);
                lblContact.Text = Convert.ToString(dr["contact"]);
                lblBirthDate.Text = Convert.ToString(dr["Birthdate"]);
                lblArrangedAt.Text = Convert.ToDateTime(dr["created_At"]).ToString("dd/MMM/yyyy hh:mm");
                lblRemark.Text = Convert.ToString(dr["remark"]);
                lblLocation.Text = Convert.ToString(dr["Location_Name"]);
                lblPosition.Text = Convert.ToString(dr["position_type"]);
                lblDepartment.Text = Convert.ToString(dr["Department_Name"]);

                lblStatus.Text = Convert.ToString(dr["isAccepted"]);


                if (string.IsNullOrEmpty(Convert.ToString(dr["isAccepted"]))) {
                    lblStatus.Text = "Not Received";
                }

                if (lblStatus.Text.Equals("Accepted"))
                {
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else {
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }

            }


        }
        catch (Exception ex)
        {

            throw ex;
        }

    }



    private DataTable GenerateTransposedTable(DataTable inputTable,string gridType)
    {
        DataTable outputTable = new DataTable();

        // Add columns by looping rows

        // Header row's first column is same as in inputTable
        //outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

        // Header row's second column onwards, 'inputTable's first column taken
        //foreach (DataRow inRow in inputTable.Rows)
        //{
        //    string newColName = inRow[0].ToString();
        //    outputTable.Columns.Add(newColName);
        //}

        if (gridType.Equals("Exam"))
        {
            outputTable.Columns.Add("Test");
            outputTable.Columns.Add("Data");
        }
        if (gridType.Equals("HR")) {
            outputTable.Columns.Add("Question");
            outputTable.Columns.Add("Answer");
        }


            // Add rows by looping columns        
            for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
        {
            DataRow newRow = outputTable.NewRow();

            // First column is inputTable's Header row's second column
            newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
            for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
            {
                string colValue = inputTable.Rows[cCount][rCount].ToString();
                newRow[cCount + 1] = colValue;
            }
            outputTable.Rows.Add(newRow);
        }

        return outputTable;
    }
}