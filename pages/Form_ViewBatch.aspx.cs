﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_Form_ViewBatch : System.Web.UI.Page
{
    string id = string.Empty;
    string batchNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]).Equals(""))
        {
            Response.Redirect("Login.aspx");
        }

        batchNo = Convert.ToString(Request.QueryString["BatchNo"]);


            if(string.IsNullOrEmpty(batchNo)) {
            Response.Redirect("Login.aspx");
        }

        if (!IsPostBack)
        {
           
            fnLoadData();

            fnLoadBatchDetails();
        }
       
    }

    private void fnLoadBatchDetails()
    {
        try
        {
            lbl_BatchName.Text = batchNo;
            lblTotalCandidate.Text = DBUtils.SqlSelectScalar(new SqlCommand("select count(*) from tbl_batch_candidate_details where [BatchNo]='" + batchNo + "'"));


            lblExamLinkSentTo.Text = DBUtils.SqlSelectScalar(new SqlCommand("select count(*) from tbl_batch_candidate_details where [BatchNo]='" + batchNo + "' and Exam_Status='"+PublicMethods.examStatus_linkSent + "'"));


            string qry = "SELECT tbl_batch_information.BatchNo,tbl_batch_information.Date,tbl_Location_Master.location_Name, tbl_Department_Master.Department_Name,     tbl_batch_information.createdBy,tbl_batch_information.remark,Case when (SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='LINK SENT') =(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo)  then 'COMPLETED' else 'INPROGRESS' end as BatchStatus FROM  tbl_batch_information INNER JOIN  tbl_Location_Master ON tbl_batch_information.location_id = tbl_Location_Master.location_Id INNER JOIN tbl_Position_Master ON tbl_batch_information.position_id = tbl_Position_Master.position_Id INNER JOIN  tbl_Department_Master ON tbl_batch_information.department_id = tbl_Department_Master.Department_Id where BatchNo='" + batchNo + "'";
            DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
            if (dt.Rows.Count > 0) {
                lblDepartment.Text = Convert.ToString(dt.Rows[0]["Department_Name"]);
                
                lbllocation.Text = Convert.ToString(dt.Rows[0]["location_Name"]);
                lblCreatedBy.Text = Convert.ToString(dt.Rows[0]["createdBy"]);
                lblCreatedAt.Text = Convert.ToString(dt.Rows[0]["Date"]);
                lblRemark.Text= Convert.ToString(dt.Rows[0]["remark"]);
                lblBatchStatus.Text = Convert.ToString(dt.Rows[0]["BatchStatus"]);
                if (lblBatchStatus.Text == "COMPLETED") {
                    lblBatchStatus.ForeColor = System.Drawing.Color.Green;
                }
            }

        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    protected void rgInterView_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            fnLoadData();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void fnLoadData()
    {
        try
        {

            //string query = "SELECT [BatchNo],[Candidate_Name],[Candidate_Email],COnvert(nvarchar(20),BirthDate,106) as [BirthDate],[age],[type],[Mobile],[Experience],[Exam_Status],id  FROM [tbl_batch_candidate_details] where BatchNo='" + batchNo + "' order by Exam_Status desc ";
            string qry = "SELECT [BatchNo],[Candidate_Name],[Candidate_Email],COnvert(nvarchar(20),BirthDate,106) as [BirthDate],[age],[type],[Mobile],[Experience],case when (select top 1[Candidate_Email] from  tbl_AssessmentTest_Data where Candidate_Email=BD.Candidate_Email) IS NULL then [Exam_Status] else 'COMPLETED' end as [Exam_Status] ,id ,[position_type]      ,[position_examId] FROM [tbl_batch_candidate_details] as BD where BD.BatchNo='" + batchNo + "'  order by Exam_Status desc";



            DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));


           
            GridView1.DataSource = dt;
            GridView1.DataBind();




           
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "view_details") {
                string email = Convert.ToString(e.CommandArgument);

                Response.Redirect("viewExamConductedDetails.aspx?email=" + email + "");
            }


            if (e.CommandName == "Select")

            {

                string deparmentId = string.Empty;
                string locationId = string.Empty;
                string positionId = string.Empty;
               
                string remark = string.Empty;
              
                string qry = "select * from [tbl_batch_information] where [BatchNo]='" + batchNo + "'";
                DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
                if (dt.Rows.Count > 0)
                {
                    deparmentId = Convert.ToString(dt.Rows[0]["department_id"]);
                    locationId = Convert.ToString(dt.Rows[0]["location_id"]);
                    positionId= Convert.ToString(dt.Rows[0]["position_id"]);
                  
                    remark = Convert.ToString(dt.Rows[0]["remark"]);
                    
                }




               
               


                    //Determine the RowIndex of the Row whose Button was clicked.
                    int rowIndex = Convert.ToInt32(e.CommandArgument);

                    //Reference the GridView Row.
                    GridViewRow row = GridView1.Rows[rowIndex];



                //Fetch value of Country
                string position_type = row.Cells[2].Text;
                String Name = row.Cells[3].Text;
                    String email = row.Cells[4].Text;
                    String birthdate = row.Cells[5].Text;
                    string age = row.Cells[6].Text;
                    string type = row.Cells[7].Text;



                    String mobile = row.Cells[8].Text;
                    String experience = row.Cells[9].Text;
                String id = row.Cells[11].Text;
               
                string position_examId = row.Cells[12].Text;








                string qryUpdate = "UPDATE [tbl_batch_candidate_details]  SET [Exam_Status] ='" + PublicMethods.examStatus_linkSent + "' where id='"+ id + "'";
                    int r = DBUtils.ExecuteSQLCommand(new SqlCommand(qryUpdate));

                if (r <= 0) {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Error while updating details'); ", true);
                    return;
                }

                    string qry1 = "INSERT INTO [tbl_JombayExam_NewInterviewMaster]([User_Email],[User_Name],[Birthdate],[age],[department_Id],[position_Id],[location_Id],[remark],contact,type,BatchNo,position_type,position_examId) VALUES ('" + email + "','" + Convert.ToString(Name).Replace("'", "''").ToString() + "','" + birthdate.ToString() + "','" + age.ToString() + "','" + deparmentId + "','" + positionId + "','" + locationId + "' ,'" + remark.Replace("'", "''").ToString() + "','" + mobile.ToString() + "','"+ type + "','" + batchNo + "','"+position_type+ "','" + position_examId + "')";





                    int i = DBUtils.ExecuteSQLCommand(new SqlCommand(qry1));
                    if (i > 0)
                    {




                        //This id used to update tbl_JombayExam_NewInterviewMaster
                        string qry_details_Id = "select MAX(Id_Exam) from [tbl_JombayExam_NewInterviewMaster]";
                        id = DBUtils.SqlSelectScalar(new SqlCommand(qry_details_Id));


                        string examLink = fnCreateExamLink(Name.Replace("'", "''"), email.ToString(), mobile.ToString(), position_examId);
                        sendExamEmail(examLink, experience, email, id);

                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Email sent'); ", true);

                    fnLoadData();

                    fnLoadBatchDetails();

                    string message = "Mail sent Successfully...";
                    string url = "Form_ViewBatch.aspx?batchNo=" + batchNo + "";
                    string script = "window.onload = function(){ alert('";
                    script += message;
                    script += "');";
                    script += "window.location = '";
                    script += url;
                    script += "'; }";
                    ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);

                  

                    }
                    else
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Error'); ", true);
                    }

                }

            
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    protected string fnCreateExamLink(string name, string email, string mobile, string positionId)
    {
        try
        {

            string CandidateName = Regex.Replace(name, @"\s+", "");
            string CandidateEmail = Regex.Replace(email, @"\s+", "");
            string CandidateMobile = Regex.Replace(mobile, @"\s+", "");
            string position_value = Regex.Replace(positionId, @"\s+", "");


            string authToken = WebConfigurationManager.AppSettings["jombay_AuthToken"];

            //string qry = "select [test_Id] from [tbl_ExamTestId] where [Title]='" + employeeType + "'";
            //string testId = DBUtils.SqlSelectScalar(new SqlCommand(qry));


            //string link = "https://assessment.jombay.com/registerCandidate?name=" + CandidateName + "&email=" + CandidateEmail + "&mobile=" + CandidateMobile + "&test_id=" + testId + "&auth_token=" + authToken + "";

            string link = "https://assessment.jombay.com/registerCandidate?name=" + CandidateName + "&email=" + CandidateEmail + "&mobile=" + CandidateMobile + "&test_id=" + position_value + "&auth_token=" + authToken + "";


            return link;


        }
        catch (Exception ex)
        {

            throw ex;
        }

    }
    protected void sendExamEmail(string url_Link, string experience, string userEmail,string eid)
    {
        try
        {




            string updateInterviewMaster = "UPDATE [tbl_JombayExam_NewInterviewMaster] SET [jombayLink] = '" + url_Link + "',arranged_By='" + Session[PublicMethods.ConstUserEmail].ToString() + "',created_At='" + DateTime.Now + "' ,Experience='" + experience + "' WHERE [Id_Exam] ='" + eid + "'  ";
            int j = DBUtils.ExecuteSQLCommand(new SqlCommand(updateInterviewMaster));



           


            string body = "";
            string emailId = "", password = "";


            SmtpClient smtp = new SmtpClient();
            smtp.Timeout = 2 * 300000;
            string qry = "SELECT * FROM tblSMTPSettingsMaster";
            DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
            foreach (DataRow dr in dt.Rows)
            {
                smtp.Host = dr["Servername"].ToString();
                smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                emailId = dr["Emailid"].ToString();
                password = dr["Usrpassword"].ToString();
            }
            smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


            System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

            mail1.From = new MailAddress(emailId, "displayName");
            mail1.Subject = "Exam Link";
            mail1.To.Add(userEmail);
            mail1.Bcc.Add(Session[PublicMethods.ConstUserEmail].ToString());
            mail1.IsBodyHtml = true;


            string bodyUser = fnFetchEMailBody(url_Link);
            mail1.Body = bodyUser;
            smtp.Send(mail1);



        }

        catch (Exception ex)
        {
            throw ex;
            // throw ex;
        }


    }


    protected string fnFetchEMailBody(string link)
    {


        string body, name = "", email = "";


        //To read html file
        StreamReader readTemplateFile = null;

        readTemplateFile = new StreamReader(Server.MapPath("Email_Body/examURLLink_Email.html"));

        string allContents = readTemplateFile.ReadToEnd();


        allContents = allContents.Replace("@#ExamLink#@", link);


        body = allContents;
        return body;

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // CHECK CONDITION.
                if ((e.Row.Cells[10].Text).ToUpper() == "COMPLETED")
                {
                    // CHANGE BACKGROUND COLOR OF THTE CELL.
                    e.Row.BackColor = System.Drawing.Color.LightGreen;
                }
                if ((e.Row.Cells[10].Text).ToUpper() == PublicMethods.examStatus_linkSent)
                {
                    // CHANGE BACKGROUND COLOR OF THTE CELL.
                    e.Row.BackColor = System.Drawing.Color.LightBlue;
                }
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
}