﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;
using unirest_net.http;
using System.Data;
using System.Configuration;
using System.Web.Script.Serialization;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
        {



        if (!IsPostBack)
        {

            if (Request.Form.Get("id_token") == null == false)
            {
                LoginButton.Enabled = true;
            }
            else
            {
                if ( Request.QueryString["page"] != null)
                {
                  
                    Session["page"] = Request.QueryString["page"];
                    //ClientScript.RegisterStartupScript(GetType(), "id", "RedirectToOffice()", true);
                }



                //string script = "function f(){$find(\"" + RW_Confirm.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";

                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
                ClientScript.RegisterStartupScript(GetType(), "id", "RedirectToOffice()", true);
                LoginButton.Enabled = false;
            }

        }




        
        
        


        if (Request.Form.Get("id_token") == null == false)
        {
            System.Net.WebClient objWebClient = new System.Net.WebClient();
            WebHeaderCollection HeaderData = new WebHeaderCollection();
            JavaScriptSerializer jSerializer = new JavaScriptSerializer();
            string ResponseString = string.Empty;
            Dictionary<string, string> ResponseData;
            string Email = string.Empty;
            string TokenID = Request.Form.Get("id_token");
            string AccessToken = Request.Form.Get("access_token");
            TokenID = TokenID.Substring(0, TokenID.IndexOf("."));
            HeaderData["Authorization"] = "Bearer " + AccessToken;

            HeaderData["client-request-id"] = ConfigurationManager.AppSettings["ida:AppId"];
            HeaderData["return-client-request-id"] = "true";
            objWebClient.Headers = HeaderData;
            ResponseString = objWebClient.DownloadString("https://outlook.office.com/api/v2.0/Me");
            ResponseData = jSerializer.Deserialize<Dictionary<string, string>>(ResponseString);
            Email = ResponseData["EmailAddress"];
            Session[PublicMethods.ConstUserEmail] = Email;

            txtUsername.Text = Email;
            txtPassword.Text = "******";

            string qry_userAccess = "select hr_email from tbl_User_Access where hr_email='" + Email + "'";
            string userAccess = DBUtils.SqlSelectScalar(new SqlCommand(qry_userAccess));

            if (userAccess == "")
            {
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Invalid Authentication,Please contact Admin'); ", true);

                ScriptManager.RegisterStartupScript(this, this.GetType(),"alert","alert('Invalid Authentication,Please contact Admin');window.location ='Login.aspx';",true);
            }
            else {


                if (Convert.ToString(Session["Page"]) == null || Convert.ToString(Session["Page"]) == "")
                {

                    Response.Redirect("ExamDashboard.aspx");
                    //ClientScript.RegisterStartupScript(GetType(), "id", "RedirectToOffice()", true);
                }
                else {
                    Response.Redirect("Form_ChangeStatus.aspx");
                }

              

            }


           
            //if(Email != string.Empty)
            //    Response.Redirect("Contact.aspx");
        }
        else {

           //  ClientScript.RegisterStartupScript(GetType(), "id", "RedirectToOffice()", true);
        }



        this.LoginButton.Attributes.Add("onclick", DisableTheButton(this.Page, this.LoginButton));
        lblBuildId.Text = "201910170933";



    }

    private void doOnce()
    {
        try
        {
            lblBuildId.Text = Application["RTC"].ToString();
            Session.RemoveAll();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void LoginButton_Click(object sender, EventArgs e)
    {
        string user = txtUsername.Text;
        string pass = txtPassword.Text;
        string userDb = "";

        if (user.Contains("@sanjeevgroup.com"))
        {
            userDb = user;
        }
        else
        {
            
            if (user.Contains("Admin"))
            {
                  user = user + "@sanjeevgroup.com";
            }
            else
            {
                string script = "alert('Invalid Credentials');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                ClientScript.RegisterStartupScript(GetType(), "id", "RedirectToOffice()", true);
                return;
            }
        }


        string qry_userAccess = "select hr_email from tbl_User_Access where hr_email='" + user + "'";
        string userAccess = DBUtils.SqlSelectScalar(new SqlCommand(qry_userAccess));
        if (user == userAccess || (user == "Admin@sanjeevgroup.com" && pass == "sanjeevgroup"))
        {
            //string valid = fnAuntheticateUserXML(user, pass);
            string valid= null;
            if (user == "Admin@sanjeevgroup.com" && pass == "sanjeevgroup")
            {

                Session[PublicMethods.ConstUserEmail] = user;
                Response.Redirect("ExamDashboard.aspx");

            }
            else
            {
                lblError.Text = "Please check User name and Password. Also internet connection is mandatory for login to work...";

                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Please check User name and Password. Also internet connection is mandatory for login to work...'); ", true);
            }
        }
        else {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Invalid Authentication,Please contact Admin'); ", true);
        
        }




    
    }

   

    public String fnAuntheticateUserXML(string user, string password)
    {
        unirest_net.http.HttpResponse<string> response = null;

        try
        {
            response = unirest_net.http.Unirest.get("https://outlook.office365.com/api/v1.0/me/")
                .basicAuth(user, password).asJson<string>();
            if (DBNulls.StringValue(response.Code) == "200")
            {
                return null;
            }
            else
            {
                return "Invalid User";
            }
        }
        catch (Exception)
        {
            return "Invalid User";
        }


    }


  

    private string DisableTheButton(Control pge, Control btn)
    {

        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        sb.Append("if (typeof(Page_ClientValidate) == 'function') {");

        sb.Append("if (Page_ClientValidate() == false) { return false; }} ");

        sb.Append("this.value = 'Please wait...';");

        sb.Append("this.disabled = true;");

        sb.Append(pge.Page.GetPostBackEventReference(btn));

        sb.Append(";");

        return sb.ToString();
    }

    protected void checkBoxAdmin_CheckedChanged(object sender, EventArgs e)
    {

    }

    

    protected void btnHR_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(GetType(), "id", "RedirectToOffice()", true);
    }

    protected void btnAdmin_Click1(object sender, EventArgs e)
    {

        LoginButton.Enabled = true;
        return;
    }
}
















