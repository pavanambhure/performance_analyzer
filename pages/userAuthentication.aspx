﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="userAuthentication.aspx.cs" Inherits="pages_userAuthentication" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <style>
        body {
            padding-top: 60px;
        }
    </style>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="style/login-register.css" rel="stylesheet" />
    <link href="style/font-awesome.css" rel="stylesheet" />
    <script src="jquery/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>
    <script src="style/login-register.js" type="text/javascript"></script>
    <title>Login</title>


    <script type="text/javascript">
        $(document).ready(function () {
            debugger;
            RedirectToOffice();
        });
 
          function RedirectToOffice() {
                debugger;
                var OfficeRedirectUrl = "  <%=ConfigurationManager.AppSettings["ida:RedirectUri"]%>";
                var Office365ClientId = "<%=ConfigurationManager.AppSettings["ida:AppId"]%>";
                var OfficeUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?response_type=id_token+token&client_id=" + Office365ClientId + "&redirect_uri=" + OfficeRedirectUrl + "&scope=openid+https%3A%2F%2Foutlook.office.com%2Fmail.read&nonce=13c9217a-d565-fb9e-d66d-d5ad18fa6f0b&response_mode=form_post"; window.open(OfficeUrl, "_self");
            }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>
