﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Web.Script.Serialization;
using System.Net;
using Telerik.Web.UI;
using System.Drawing;

public partial class pages_ExamDashboard : System.Web.UI.Page
{
    string typeId = string.Empty;
    string createdBy = string.Empty;
    string UserId;
    string fresherLabel = string.Empty;
    string EmployeeLabel = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

      



        //Check Login
        if (DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]).Equals(""))
        {
            Response.Redirect("Login.aspx");
        }

        if (Session[PublicMethods.ConstUserEmail].ToString() == "Admin@sanjeevgroup.com") {

            newInterview1.HRef = "#";
            newInterview.HRef = "#";
            newInterview1.Style.Add("background-color", "#81867f");
        }

        if (!IsPostBack)
        {
            fnLoadData(true);
           
        }

        fnTabValue();

        fresherLabel = "0 TO " + ConfigurationManager.AppSettings["fresherExpUpto"] +" " + "yrs";
        EmployeeLabel = ConfigurationManager.AppSettings["fresherExpUpto"] + "+" + " " + "yrs";
     
    
    }

    private void fnTabValue()
    {
        try
        {
            string qry = "select count(*) from tbl_batch_information";
            lblTotalBatches.Text = DBUtils.SqlSelectScalar(new SqlCommand(qry));



            //urgent requirement,changes need to modify 
            qry = "select count(*) from (SELECT tbl_batch_information.BatchNo,tbl_batch_information.Date,LEFT(tbl_batch_information.createdBy, CHARINDEX('@', tbl_batch_information.createdBy) - 1) AS createdBy,tbl_batch_information.created_at, tbl_batch_information.department_id, tbl_batch_information.position_id,                        tbl_batch_information.location_id, tbl_batch_information.uploaded_file_name, tbl_Department_Master.Department_Name, tbl_Position_Master.position_Name, tbl_Location_Master.location_Name,( Convert(nvarchar,(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='" + PublicMethods.examStatus_linkSent + "'))+'/' + Convert(nvarchar,(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo)) ) as Total_Candidate, Case when (SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='LINK SENT') =(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo)  then 'COMPLETED' when (SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='" + PublicMethods.examStatus_linkSent + "')=0 then 'CREATED' else 'INPROGRESS' end as BatchStatus  FROM tbl_batch_information INNER JOIN  tbl_Department_Master ON tbl_batch_information.department_id = tbl_Department_Master.Department_Id INNER JOIN                         tbl_Location_Master ON tbl_batch_information.location_id = tbl_Location_Master.location_Id INNER JOIN  tbl_Position_Master ON tbl_batch_information.position_id = tbl_Position_Master.position_Id ) as t where t.BatchStatus='INPROGRESS'   ";
            lblInprogressBatch.Text = DBUtils.SqlSelectScalar(new SqlCommand(qry));



            qry = "select count(*) from (SELECT tbl_batch_information.BatchNo,tbl_batch_information.Date,LEFT(tbl_batch_information.createdBy, CHARINDEX('@', tbl_batch_information.createdBy) - 1) AS createdBy,tbl_batch_information.created_at, tbl_batch_information.department_id, tbl_batch_information.position_id,                        tbl_batch_information.location_id, tbl_batch_information.uploaded_file_name, tbl_Department_Master.Department_Name, tbl_Position_Master.position_Name, tbl_Location_Master.location_Name,( Convert(nvarchar,(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='" + PublicMethods.examStatus_linkSent + "'))+'/' + Convert(nvarchar,(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo)) ) as Total_Candidate, Case when (SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='LINK SENT') =(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo)  then 'COMPLETED' when (SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='" + PublicMethods.examStatus_linkSent + "')=0 then 'CREATED' else 'INPROGRESS' end as BatchStatus  FROM tbl_batch_information INNER JOIN  tbl_Department_Master ON tbl_batch_information.department_id = tbl_Department_Master.Department_Id INNER JOIN                         tbl_Location_Master ON tbl_batch_information.location_id = tbl_Location_Master.location_Id INNER JOIN  tbl_Position_Master ON tbl_batch_information.position_id = tbl_Position_Master.position_Id ) as t where t.BatchStatus='COMPLETED'   ";
            lblCompleted.Text = DBUtils.SqlSelectScalar(new SqlCommand(qry));


        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    private void fnLoadData(Boolean DoRebind)
    {
        try
        {


            


                string query = "select * from (SELECT tbl_batch_information.BatchNo,tbl_batch_information.Date,LEFT(tbl_batch_information.createdBy, CHARINDEX('@', tbl_batch_information.createdBy) - 1) AS createdBy,tbl_batch_information.created_at, tbl_batch_information.department_id,                        tbl_batch_information.location_id, tbl_batch_information.uploaded_file_name, tbl_Department_Master.Department_Name , tbl_Location_Master.location_Name,( Convert(nvarchar,(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='" + PublicMethods.examStatus_linkSent + "'))+'/' + Convert(nvarchar,(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo)) ) as Total_Candidate, Case when (SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='LINK SENT') =(SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo)  then 'COMPLETED' when (SELECT Count(*) FROM [tbl_batch_candidate_details] where [BatchNo] =tbl_batch_information.BatchNo and Exam_Status='"+ PublicMethods.examStatus_linkSent + "')=0 then 'CREATED' else 'INPROGRESS' end as BatchStatus  FROM tbl_batch_information INNER JOIN  tbl_Department_Master ON tbl_batch_information.department_id = tbl_Department_Master.Department_Id INNER JOIN                         tbl_Location_Master ON tbl_batch_information.location_id = tbl_Location_Master.location_Id INNER JOIN  tbl_Position_Master ON tbl_batch_information.position_id = tbl_Position_Master.position_Id ) as t where t.BatchStatus='" + ddlStatus.SelectedValue.ToString()+"'   order by t.created_at desc";

            DataTable dt = DBUtils.SQLSelect(new SqlCommand(query));

            rgCreatedBatch.DataSource = dt;
            if (DoRebind == true)
                rgCreatedBatch.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    protected void rgCreatedBatch_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            fnLoadData(false);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }





    protected void rgCreatedBatch_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                
                if (dataBoundItem["BatchStatus"].Text == "COMPLETED")
                {

                    //dataBoundItem["BatchStatus"].BackColor = Color.LightGreen;
                    e.Item.BackColor = System.Drawing.Color.LightGreen; //
                }
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        try
        {
            fnLoadData(true);
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
}