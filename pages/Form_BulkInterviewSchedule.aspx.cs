﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_Form_BulkInterviewSchedule : System.Web.UI.Page
{
    string uploadedCsvFileName = string.Empty;

    string id = string.Empty;
    DataTable dtCsv = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DBNulls.StringValue(Session[PublicMethods.ConstUserEmail]).Equals(""))
        {
            Response.Redirect("Login.aspx");
        }

      
        if (!IsPostBack)
        {
            fnPopulateDepartment();
            fnPopulatePosition();
            fnPopulateLocation();

        }
    }

    protected void fnPopulateDepartment()
    {
        try
        {
            string query = "select * from tbl_Department_Master";
            DataTable dtDept = DBUtils.SQLSelect(new SqlCommand(query));
            rddlDept.DataTextField = "Department_Name";
            rddlDept.DataValueField = "Department_Id";
            rddlDept.DataSource = dtDept;
            rddlDept.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    protected void fnPopulatePosition()
    {
        try
        {
            string query = "select * from tbl_Position_Master";
            DataTable dtPos = DBUtils.SQLSelect(new SqlCommand(query));
            rddlPosition.DataTextField = "position_Name";
            rddlPosition.DataValueField = "position_Id";
            rddlPosition.DataSource = dtPos;
            rddlPosition.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    protected void fnPopulateLocation()
    {
        try
        {
            string query = "select * from tbl_Location_Master";
            DataTable dtLoc = DBUtils.SQLSelect(new SqlCommand(query));
            rddlLocation.DataTextField = "location_Name";
            rddlLocation.DataValueField = "location_Id";
            rddlLocation.DataSource = dtLoc;
            rddlLocation.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = ReadCsvFile();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            lblerror.Text = ex.Message;
        }
    }
    public DataTable ReadCsvFile()
    {

       
        string Fulltext;
        if (FileUpload1.HasFile)
        {

            string user = Convert.ToString(Session[PublicMethods.ConstUserEmail]).Substring(0, Convert.ToString(Session[PublicMethods.ConstUserEmail]).IndexOf("@"));
            string FileSaveWithPath = Server.MapPath("\\CSV\\Onboarder_" + user + "_" + System.DateTime.Now.ToString("ddMMMyyyy_hhmm") + ".csv");
            Session["uploadedCsvFileName"] = "Onboarder_" + user + "_" + System.DateTime.Now.ToString("ddMMMyyyy_hhmm") + ".csv";
            FileUpload1.SaveAs(FileSaveWithPath);
            using (StreamReader sr = new StreamReader(FileSaveWithPath))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtCsv.Columns.Add(rowValues[j]); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString();
                                }
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }
        }
        return dtCsv;
    }

    protected void btnScheduleInterview_Click(object sender, EventArgs e)
    {
        if (GridView1.Rows.Count > 0)
        {
            string script = "function f(){$find(\"" + RW_Approve.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
        }
        else
        {


            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Data found');", true);
            return;
        }
    
   

    }

    protected void btnDownloadSampleExcel_Click(object sender, EventArgs e)
    {
        try
        {
            string path = "/CSV/Sample/Onboarder_Candidate.csv";

            if (System.IO.File.Exists(Server.MapPath(path)))
            {

            

                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
            string filePath = path;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=onBoarder_sample.csv");
                byte[] data = req.DownloadData(Server.MapPath(filePath));
                response.BinaryWrite(data);
                response.End();
            }
            else
            {
                lblerror.Text = "File not found";
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }



    protected string fnCreateExamLink(string name, string email, string mobile, string employeeType)
    {
        try
        {

            string CandidateName = Regex.Replace(name, @"\s+", "");
            string CandidateEmail = Regex.Replace(email, @"\s+", "");
            string CandidateMobile = Regex.Replace(mobile, @"\s+", "");
            string CandidateType = Regex.Replace(employeeType, @"\s+", "");


            string authToken = WebConfigurationManager.AppSettings["jombay_AuthToken"];

            string qry = "select [test_Id] from [tbl_ExamTestId] where [Title]='" + employeeType + "'";
            string testId = DBUtils.SqlSelectScalar(new SqlCommand(qry));


            string link = "https://assessment.jombay.com/registerCandidate?name=" + CandidateName + "&email=" + CandidateEmail + "&mobile=" + CandidateMobile + "&test_id=" + testId + "&auth_token=" + authToken + "";


            return link;


        }
        catch (Exception ex)
        {

            throw ex;
        }

    }





    protected void sendExamEmail(string url_Link,string experience,string userEmail)
    {
        try
        {
            



            string updateInterviewMaster = "UPDATE [tbl_JombayExam_NewInterviewMaster] SET [jombayLink] = '" + url_Link + "',arranged_By='" + Session[PublicMethods.ConstUserEmail].ToString() + "',created_At='" + DateTime.Now + "' ,Experience='" + experience + "' WHERE [Id_Exam] ='" + id + "'  ";
            int j = DBUtils.ExecuteSQLCommand(new SqlCommand(updateInterviewMaster));



            //   string qryToaddExp = "INSERT into tbl_Experience_Master(User_Email,Experience,url_Id,isAccepted) values ('" + Session["Email"].ToString() + "','" + Session["exp"].ToString() + "','" + url_Id + "','') ";
            //int a=  DBUtils.ExecuteSQLCommand(new SqlCommand(qryToaddExp));


            string body = "";
            string emailId = "", password = "";


            SmtpClient smtp = new SmtpClient();
            smtp.Timeout = 2 * 300000;
            string qry = "SELECT * FROM tblSMTPSettingsMaster";
            DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
            foreach (DataRow dr in dt.Rows)
            {
                smtp.Host = dr["Servername"].ToString();
                smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                emailId = dr["Emailid"].ToString();
                password = dr["Usrpassword"].ToString();
            }
            smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


            System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

            mail1.From = new MailAddress(emailId, "displayName");
            mail1.Subject = "Exam Link";
            mail1.To.Add(userEmail);
            mail1.Bcc.Add(Session[PublicMethods.ConstUserEmail].ToString());
            mail1.IsBodyHtml = true;


            string bodyUser = fnFetchEMailBody(url_Link);
            mail1.Body = bodyUser;
            smtp.Send(mail1);

          

        }

        catch (Exception ex)
        {
            throw ex;
            // throw ex;
        }


    }


    protected string fnFetchEMailBody(string link)
    {


        string body, name = "", email = "";


        //To read html file
        StreamReader readTemplateFile = null;

        readTemplateFile = new StreamReader(Server.MapPath("Email_Body/examURLLink_Email.html"));

        string allContents = readTemplateFile.ReadToEnd();


        allContents = allContents.Replace("@#ExamLink#@", link);
        

        body = allContents;
        return body;

    }

    protected void btnYes_Click(object sender, EventArgs e)
    {
        try
        {




            foreach (GridViewRow row in GridView1.Rows)
            {
                // here you'll get all rows with RowType=DataRow
                // others like Header are omitted in a foreach

                String Name = row.Cells[1].Text;
                String email = row.Cells[2].Text;
                String birthdate = row.Cells[3].Text;
                var today = DateTime.Today;
                // Calculate the age.
                var age = today.Year - Convert.ToDateTime(birthdate).Year;
                if (Convert.ToDateTime(birthdate).Date > today.AddYears(-age)) age--;



                String mobile = row.Cells[4].Text;
                String experience = row.Cells[5].Text;


                Double exp = Convert.ToDouble(experience);  //user input
                Double definedExp = Convert.ToDouble(ConfigurationManager.AppSettings["fresherExpUpto"]);  //Exp from Web.config

                string type = string.Empty;
                if (exp <= definedExp)
                {
                    type = "Fresher";

                }
                else
                {
                    type = "Employee";
                }

                //if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(birthdate) || string.IsNullOrEmpty(mobile) || string.IsNullOrEmpty(experience))
                //{

                //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Name,Email,birthdate,mobile,experience are mandatory'); ", true);
                //}



                string qry = "INSERT INTO [tbl_JombayExam_NewInterviewMaster]([User_Email],[User_Name],[Birthdate],[age],[department_Id],[position_Id],[location_Id],[remark],contact,examConductedType,BulkUploadedfileName) VALUES ('" + email + "','" + Convert.ToString(Name).Replace("'", "''").ToString() + "','" + birthdate.ToString() + "','" + age.ToString() + "','" + rddlDept.SelectedValue.ToString() + "','" + rddlPosition.SelectedValue.ToString() + "','" + rddlLocation.SelectedValue.ToString() + "' ,'" + txtRemark.Text.Replace("'", "''").ToString() + "','" + mobile.ToString() + "','" + PublicMethods.examCondutedType_BULK + "','" + Convert.ToString(Session["uploadedCsvFileName"]) + "')";





                int i = DBUtils.ExecuteSQLCommand(new SqlCommand(qry));
                if (i > 0)
                {




                    //This id used to update tbl_JombayExam_NewInterviewMaster
                    string qry_details_Id = "select MAX(Id_Exam) from [tbl_JombayExam_NewInterviewMaster]";
                    id = DBUtils.SqlSelectScalar(new SqlCommand(qry_details_Id));


                    string examLink = fnCreateExamLink(Name.Replace("'", "''"), email.ToString(), mobile.ToString(), type);
                    sendExamEmail(examLink, experience, email);




                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Error'); ", true);
                }


            }



            string message = " Data saved Successfully...";
            string url = "ExamDashboard.aspx";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "');";
            script += "window.location = '";
            script += url;
            script += "'; }";
            ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);


        }
        catch (Exception ex)
        {

            throw ex;
        }
    
    }

    protected void cancelClick_Click(object sender, EventArgs e)
    {
        return;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
             if (e.CommandName == "Select")

    {


                if (rddlDept.SelectedValue == "") {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Select Department'); ", true);
                    return;
                }
                if (rddlPosition.SelectedValue == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Select Position'); ", true);
                    return;
                }

                if (rddlLocation.SelectedValue == "")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Select Location'); ", true);
                    return;
                }
               


                //Determine the RowIndex of the Row whose Button was clicked.
                int rowIndex = Convert.ToInt32(e.CommandArgument);
 
        //Reference the GridView Row.
        GridViewRow row = GridView1.Rows[rowIndex];
 
      
 
        //Fetch value of Country
       
                String Name = row.Cells[1].Text;
                String email = row.Cells[2].Text;
                String birthdate = row.Cells[3].Text;
                var today = DateTime.Today;
                // Calculate the age.
                var age = today.Year - Convert.ToDateTime(birthdate).Year;
                if (Convert.ToDateTime(birthdate).Date > today.AddYears(-age)) age--;



                String mobile = row.Cells[4].Text;
                String experience = row.Cells[5].Text;


                Double exp = Convert.ToDouble(experience);  //user input
                Double definedExp = Convert.ToDouble(ConfigurationManager.AppSettings["fresherExpUpto"]);  //Exp from Web.config

                string type = string.Empty;
                if (exp <= definedExp)
                {
                    type = "Fresher";

                }
                else
                {
                    type = "Employee";
                }

                string qry = "INSERT INTO [tbl_JombayExam_NewInterviewMaster]([User_Email],[User_Name],[Birthdate],[age],[department_Id],[position_Id],[location_Id],[remark],contact,examConductedType,BulkUploadedfileName) VALUES ('" + email + "','" + Convert.ToString(Name).Replace("'", "''").ToString() + "','" + birthdate.ToString() + "','" + age.ToString() + "','" + rddlDept.SelectedValue.ToString() + "','" + rddlPosition.SelectedValue.ToString() + "','" + rddlLocation.SelectedValue.ToString() + "' ,'" + txtRemark.Text.Replace("'", "''").ToString() + "','" + mobile.ToString() + "','" + PublicMethods.examCondutedType_BULK + "','" +Convert.ToString(Session["uploadedCsvFileName"]) +"')";





                int i = DBUtils.ExecuteSQLCommand(new SqlCommand(qry));
                if (i > 0)
                {




                    //This id used to update tbl_JombayExam_NewInterviewMaster
                    string qry_details_Id = "select MAX(Id_Exam) from [tbl_JombayExam_NewInterviewMaster]";
                    id = DBUtils.SqlSelectScalar(new SqlCommand(qry_details_Id));


                    string examLink = fnCreateExamLink(Name.Replace("'", "''"), email.ToString(), mobile.ToString(), type);
                    sendExamEmail(examLink, experience, email);

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Email sent'); ", true);

                    row.Visible = false;
                    return;
                   
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "alert('Error'); ", true);
                }

              
    }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    protected void btnDone_Click(object sender, EventArgs e)
    {
        Response.Redirect("ExamDashboard.aspx");
    }
}