﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_form_thankyou : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string url = Request.QueryString["url_id"];
        string status = Request.QueryString["status"];

        string q = "select  isAccepted from tbl_JombayExam_NewInterviewMaster where Id=" + url + "";
        string res = DBUtils.SqlSelectScalar(new SqlCommand(q));

        if (res == "")
        {
            status1.Text = "Status:" + status;
        }
        else
        {



            status1.Text = "Status:" + res;
        }
        if (res == "" || res == null)
        {
            string qry = "UPDATE  tbl_JombayExam_NewInterviewMaster SET [isAccepted] = '" + status + "' where Id='" + url + "' ";
            int i = DBUtils.ExecuteSQLCommand(new SqlCommand(qry));
        }
        else {
            lbl1.Text = "Feedback is already submitted"; 
        }
        fnGetDetails();
    }

    private void fnGetDetails()
    {
        try
        {
            //string qry = "SELECT     tbl_Interview_Master.User_Id, tbl_Interview_Master.User_Name, tbl_Interview_Master.User_Email, tbl_Interview_Master.User_Name AS Expr1, CONVERT(VARCHAR(15),tbl_Interview_Master.Birthdate, 106) AS Birthdate, tbl_Interview_Master.age, tbl_Interview_Master.department_Id, tbl_Interview_Master.position_Id, tbl_Interview_Master.location_Id,tbl_Interview_Master.remark, tbl_Interview_Master.url_Id, SUBSTRING(tbl_Interview_Master.arranged_By, 0, CHARINDEX('@', tbl_Interview_Master.arranged_By)) AS arranged_By,tbl_Interview_Master.created_At, tbl_Interview_Master.contact, tbl_Department_Master.Department_Name, tbl_Location_Master.location_Name, tbl_Position_Master.position_Name,tbl_Exam_Master.url_Link, tbl_Interview_Master.type, tbl_Experience_Master.isAccepted ,  tbl_Experience_Master.Experience FROM         tbl_Interview_Master INNER JOIN tbl_Department_Master ON tbl_Interview_Master.department_Id = tbl_Department_Master.Department_Id INNER JOIN tbl_Location_Master ON tbl_Interview_Master.location_Id = tbl_Location_Master.location_Id INNER JOIN tbl_Position_Master ON tbl_Interview_Master.position_Id = tbl_Position_Master.position_Id INNER JOIN tbl_Exam_Master ON tbl_Interview_Master.url_Id = tbl_Exam_Master.url_Id INNER JOIN tbl_Experience_Master ON tbl_Interview_Master.url_Id = tbl_Experience_Master.url_Id  where  tbl_Interview_Master.url_Id ='"+Request.QueryString["url_id"]+"'  ";



            string qry = "SELECT [Id],[User_Email],[User_Name],[Birthdate],[age],[remark],[jombayLink],[arranged_By],[created_At],[contact],[type],[Experience],[isAccepted],(select Department_Name from [tbl_Department_Master] where [department_Id]=[tbl_JombayExam_NewInterviewMaster].[department_Id]) as Department_Name,(select position_Name from [tbl_Position_Master] where [position_Id]=[tbl_JombayExam_NewInterviewMaster].[position_Id]) as position_Name,(select [location_Name] from tbl_Location_Master where [location_Id]=[tbl_JombayExam_NewInterviewMaster].[location_Id]) as location_Name, Case when (select TOP 1 ReportLink from tbl_AssessmentTest_Data where Candidate_Email=tbl_JombayExam_NewInterviewMaster.User_Email)  IS NULL THEN '#' ELSE (select TOP 1 ReportLink from tbl_AssessmentTest_Data where Candidate_Email=tbl_JombayExam_NewInterviewMaster.User_Email) end as Report_Link FROM [tbl_JombayExam_NewInterviewMaster]  where Id= '" + Request.QueryString["url_id"] + "'";
            DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
            foreach (DataRow  dr in dt.Rows)
            {
                lblEmail.Text = dr["User_Email"].ToString();
                lblBirth.Text = dr["Birthdate"].ToString();
                lblAge.Text = dr["age"].ToString();
                lblLocation.Text = dr["location_Name"].ToString();
                lblPosition.Text = dr["position_Name"].ToString();
                lblDeparment.Text = dr["Department_Name"].ToString();
                lblExperience.Text = dr["Experience"].ToString();
                lblArrangedBy.Text = dr["arranged_By"].ToString();
                lblCreatedAt.Text = dr["created_At"].ToString();
                lblUrl.Text = dr["jombayLink"].ToString();
                lblName.Text = dr["User_Name"].ToString();

                reportLink.HRef= dr["Report_Link"].ToString();
            }
        }
        catch (Exception)
        {
            
            throw;
        }
    }
}