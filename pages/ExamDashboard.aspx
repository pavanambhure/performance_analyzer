﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/examMaster.master" AutoEventWireup="true" CodeFile="ExamDashboard.aspx.cs" Inherits="pages_ExamDashboard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Dashboard</title>
    <script src="style/d3.v3.min.js" language="JavaScript"></script>
    <script src="style/liquidFillGauge.js" language="JavaScript"></script>

     
    <style>
        .liquidFillGaugeText
        {
            font-family: Helvetica;
            font-weight: bold;
        }
    </style>


    <script type="text/javascript" language="javascript">
        function disableBackButton() {
            window.history.forward()
        }
        disableBackButton();
        window.onload = disableBackButton();
        window.onpageshow = function (evt) { if (evt.persisted) disableBackButton() }
        window.onunload = function () { void (0) }
    </script>

    <script>
        window.location.hash = "no-back-button";
        window.location.hash = "Again-No-back-button";//again because google chrome don't insert first hash into history
        window.onhashchange = function () { window.location.hash = "no-back-button"; }
</script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyStart" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body1" runat="Server">
    

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body2" runat="Server">
     <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-sm-3">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <center> 
                                    <a href="Form_CreateBatch.aspx" onclick="skm_LockScreen('We are processing your request...')"  class="btn btn-default btn-login" style="height:64px; background-color:#563d7c; color:white; width:170px; font-size:25px;"  id="newInterview1" runat="server">New</a>
								</center>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-slice"></i><a href="Form_CreateBatch.aspx" style="color: black;"  onclick="skm_LockScreen('We are processing your request...')" id="newInterview" runat="server"><B>New Batch</B></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-lg-3 col-sm-3">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <center> 
                                    <a href="Form_BulkInterviewReport.aspx" onclick="skm_LockScreen('We are processing your request...')"  class="btn btn-default btn-login" style="height:64px; background-color:#563d7c; color:white; width:170px; font-size:25px;"  id="A1" runat="server">Report</a>
								</center>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-book"></i><a href="Form_BulkInterviewReport.aspx" style="color: black;"  onclick="skm_LockScreen('We are processing your request...')" id="A2" runat="server"><B>All Batches Report</B></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-2 col-sm-2">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <center> 
                                   <asp:Label Font-Bold="true" ID="lblTotalBatches"  Style="font-size:x-large" runat="server" Text="-" ></asp:Label>
								</center>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-book"></i><a href="#" style="color: black;"  onclick="skm_LockScreen('We are processing your request...')" id="A4" runat="server"><B>Total Batches</B></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                  <div class="col-lg-2 col-sm-2">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <center> 
                                    <asp:Label Font-Bold="true" ID="lblInprogressBatch"  Style="font-size:x-large" runat="server" Text="-" ></asp:Label>
								</center>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-book"></i><a href="#" style="color: black;"  onclick="skm_LockScreen('We are processing your request...')" id="A3" runat="server"><B>Inprogress Batches</B></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                  <div class="col-lg-2 col-sm-2">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <center> 
                                    <asp:Label Font-Bold="true" ID="lblCompleted"  Style="font-size:x-large" runat="server" Text="-" ></asp:Label>
								</center>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-book"></i><a href="#" style="color: black;"  onclick="skm_LockScreen('We are processing your request...')" id="A5" runat="server"><B>Completed Batches</B></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 
                </div>

               <%--  <div class="col-lg-4 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <center> 
                                    <a href="JombayIntegration_New_Interview_report.aspx" class="btn btn-default btn-info" style="height:64px; color:black;background-color:dodgerblue; width:170px; font-size:25px;"  >
                                        <asp:Label Font-Bold="true" ID="lbl_IncompleteExam"  Style="font-size:25px" runat="server" Text="0"></asp:Label>

                                    </a>

                                    
								</center>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-info"></i><B style="color: black;cursor:context-menu" >Incomplete Exam</B>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>

           




            </div>
          <div class="row">
              <div class="card">
               <div class="content">
                  <div class="header">
                                        <h4 class="title" style="color:white">Created Batch</h4>
                                      
                                    </div>
                 
                                    <div class="content table-responsive table-full-width" >
                                          
                                        <table> <tr>
                                            <td>
                                                  <asp:DropDownList  CssClass="form-control" ID="ddlStatus" runat="server" Width="100%" BackColor="LightGray">
      <asp:ListItem Text="CREATED" Value="CREATED"></asp:ListItem>
    <asp:ListItem Text="INPROGRESS" Value="INPROGRESS"></asp:ListItem>
    <asp:ListItem Text="COMPLETED" Value="COMPLETED"></asp:ListItem>
   
   
</asp:DropDownList>
                                       
                                            </td>
                                            <td>
                                                 <asp:Button ID="btnApply" CssClass="btn btn-fill" runat="server" Text="Apply"  OnClick="btnApply_Click" />
                                            </td>
                                                </tr></table>
                                      

                                        <br />

                                        <div style="overflow:scroll">

                                        <telerik:radgrid id="rgCreatedBatch" runat="server" allowfilteringbycolumn="true" AutoGenerateColumns="false"  OnNeedDataSource="rgCreatedBatch_NeedDataSource" Skin="Bootstrap"  OnItemDataBound="rgCreatedBatch_ItemDataBound"     >
                                            <GroupingSettings CollapseAllTooltip="Collapse all groups" CaseSensitive="false"></GroupingSettings>
                                                 <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="Created Batch"> </ExportSettings>
                                            <MasterTableView CommandItemDisplay="Top">
                                                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                <Columns>


                                                  

                                                    
                                                  

                                                  <telerik:GridHyperLinkColumn  HeaderStyle-Font-Bold="true" DataNavigateUrlFields="BatchNo" DataType="System.String" 
                                   DataNavigateUrlFormatString="Form_ViewBatch.aspx?BatchNo={0}" HeaderText="Batch No" AutoPostBackOnFilter="false"  ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"   HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle"  ItemStyle-ForeColor="CadetBlue" ItemStyle-Font-Size="Large" FilterCheckListEnableLoadOnDemand="true" DataTextField="BatchNo" ItemStyle-Font-Underline="true" UniqueName="view"  HeaderStyle-Width="150px" ></telerik:GridHyperLinkColumn> 

                                                   

                                                     <telerik:GridBoundColumn Visible="true" HeaderStyle-HorizontalAlign="Center"  DataField="BatchStatus" HeaderText="Batch Status" ItemStyle-HorizontalAlign="Center" DataType="System.String"   HeaderStyle-Width="150px"  ></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn Visible="true" HeaderStyle-HorizontalAlign="Center"  DataField="Total_Candidate" HeaderText="Exam Link sent to/Total Candidate" ItemStyle-HorizontalAlign="Center" DataType="System.String"   HeaderStyle-Width="150px"  ></telerik:GridBoundColumn>
                                                     <telerik:GridBoundColumn Visible="true" HeaderStyle-HorizontalAlign="Center"  DataField="Date" HeaderText="Date" ItemStyle-HorizontalAlign="Center" DataType="System.String"   HeaderStyle-Width="150px"  ></telerik:GridBoundColumn>

                                                       <telerik:GridBoundColumn Visible="true" HeaderStyle-HorizontalAlign="Center"  DataField="createdBy" HeaderText="Created By" ItemStyle-HorizontalAlign="Center" DataType="System.String"   HeaderStyle-Width="150px"  ></telerik:GridBoundColumn>
                                          
                                                    
                                                     <telerik:GridBoundColumn Visible="true" HeaderStyle-HorizontalAlign="Center"  DataField="Department_Name" HeaderText="Department" ItemStyle-HorizontalAlign="Center" DataType="System.String"   HeaderStyle-Width="200px"  ></telerik:GridBoundColumn>
                                          
                                                   
                                                       <telerik:GridBoundColumn Visible="true" HeaderStyle-HorizontalAlign="Center"  DataField="location_Name" HeaderText="Location" ItemStyle-HorizontalAlign="Center" DataType="System.String"   HeaderStyle-Width="150px"  ></telerik:GridBoundColumn>
                                                     
                                                    
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings>
                                               
                                                  <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
                                            </ClientSettings>
                                        </telerik:radgrid>



                                            </div>





                                    </div>
                   </div>
                  </div>
                 </div>

      

       

        
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Footer" runat="Server">
</asp:Content>

